FROM ruby:2.4.0
MAINTAINER cos <us@cos.ovh>
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN git clone https://gitlab.com/cospoland/feedem /app
WORKDIR /app
RUN bundle install
CMD ["bundle", "exec", "rails", "s", "-p", "80", "-b", "0.0.0.0"]
